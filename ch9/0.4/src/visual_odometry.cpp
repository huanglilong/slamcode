#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <algorithm>
#include <boost/timer.hpp>

#include "myslam/config.h"
#include "myslam/visual_odometry.h"
#include "myslam/g2o_types.h"

namespace myslam
{
VisualOdometry::VisualOdometry():
    state_ ( INITIAZING ), ref_ ( nullptr ), curr_ ( nullptr ), map_ ( new Map ), num_lost_ ( 0 ), num_inliers_ ( 0 ), matcher_flann_ ( new cv::flann::LshIndexParams ( 5,10,2 ) )
{
    num_of_features_    = Config::get<int> ( "number_of_features" );
    scale_factor_       = Config::get<double> ( "scale_factor" );
    level_pyramid_      = Config::get<int> ( "level_pyramid" );
    match_ratio_        = Config::get<float> ( "match_ratio" );
    max_num_lost_       = Config::get<float> ( "max_num_lost" );
    min_inliers_        = Config::get<int> ( "min_inliers" );
    key_frame_min_rot   = Config::get<double> ( "keyframe_rotation" );
    key_frame_min_trans = Config::get<double> ( "keyframe_translation" );
    map_point_erase_ratio_ = Config::get<double> ( "map_point_erase_ratio" );
    orb_ = cv::ORB::create (num_of_features_, scale_factor_, level_pyramid_);
}

VisualOdometry::~VisualOdometry() {}

bool VisualOdometry::addFrame(Frame::Ptr frame)
{
    switch(state_)
    {
        case INITIAZING:
        {
            state_ = OK;
            curr_ = ref_ = frame;               // first image use as reference frame
            extractKeyPoints();                 // extract current image's keypoints
            computeDescriptors();               // compute current image keypoints's descritptor
            addKeyFrame();                      // the first frame is a keyframe, and add all keypoints to mappoint
            break;
        }
        case OK:
        {
            curr_ = frame;
            curr_->T_c_w_ = ref_->T_c_w_;       // use prev(ref_) T as current T's initial guess for checking in the frame
            extractKeyPoints();
            computeDescriptors();
            featureMatching();                  // match current frame keypoints with map
            poseEstimationPnP();                // estimate current frame's pose within the map
            Mat orb_img;
            drawKeypoints(curr_->color_, keypoints_curr_, orb_img, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);
            imshow("ORB Features", orb_img);
            if(checkEstimatedPose() == true)    // a good estimation
            {
                curr_->T_c_w_ = T_c_w_estimated_;
                optimizeMap();
                num_lost_ = 0;
                if(checkKeyFrame() == true)     // is a keyframe
                {
                    addKeyFrame();
                }
            }
            else    // bad estimation due to various reasons
            {
                num_lost_++;
                if(num_lost_ > max_num_lost_)
                {
                    state_ = LOST;
                }
                return false;
            }
            break;
        }
        case LOST:
        {
            cout << "vo has lost." << endl;
            break;
        }
    }
    return true;
}

void VisualOdometry::extractKeyPoints()
{
    orb_->detect ( curr_->color_, keypoints_curr_ );
}

void VisualOdometry::computeDescriptors()
{
    orb_->compute ( curr_->color_, keypoints_curr_, descriptors_curr_ );
}

void VisualOdometry::featureMatching()
{
    // match desp_ref and desp_curr
    vector<cv::DMatch> matches;
    // select the candidates in map
    Mat desp_map;
    vector<MapPoint::Ptr> candidate;
    for(auto& allpoints:map_->map_points_)
    {
        MapPoint::Ptr& p = allpoints.second;
        // check if p in curr frame image,
        // use this part(in the current frame) keypoints in map and matching with current frame's keypoints
        if(curr_->isInFrame(p->pos_))
        {
            // add to candidate
            p->visible_times_++;
            candidate.push_back(p);
            desp_map.push_back(p->descriptor_);
        }
    }

    matcher_flann_.match(desp_map, descriptors_curr_, matches);
    // select the best matches
    float min_dis = std::min_element (
                        matches.begin(), matches.end(),
                        [] ( const cv::DMatch& m1, const cv::DMatch& m2 )
    {
        return m1.distance < m2.distance;
    } )->distance;
    
    match_3dpts_.clear();           // features(landmarks)
    match_2dkp_index_.clear();      // current frame matching index with map
    for(cv::DMatch& m:matches)
    {
        if(m.distance < max<float> (min_dis*match_ratio_, 30.0))
        {
            match_3dpts_.push_back(candidate[m.queryIdx]);  // mappoints in the current frame
            match_2dkp_index_.push_back(m.trainIdx);        // current frame keypoint's matching index
        }
    }
    cout << "good matches: " << match_3dpts_.size() << endl;
}

void VisualOdometry::poseEstimationPnP()
{
    // construct the 3d 2d observations
    vector<cv::Point3f> pts3d;
    vector<cv::Point2f> pts2d;

    for(int index:match_2dkp_index_)
    {
        pts2d.push_back(keypoints_curr_[index].pt); // in pixel frame
    }
    for(MapPoint::Ptr pt:match_3dpts_)
    {
        pts3d.push_back(pt->getPositionCV());       // in world frame
    }
    Mat K = ( cv::Mat_<double>(3,3)<<
        ref_->camera_->fx_, 0, ref_->camera_->cx_,
        0, ref_->camera_->fy_, ref_->camera_->cy_,
        0,0,1
    );

    Mat rvec, tvec, inliers;
    cv::solvePnPRansac(pts3d, pts2d, K, Mat(), rvec, tvec, false, 100, 4.0, 0.99, inliers);
    num_inliers_ = inliers.rows;
    cout << "PnP inliers: " << num_inliers_ << endl;
    T_c_w_estimated_ = SE3(
        SO3(rvec.at<double>(0,0), rvec.at<double>(1,0), rvec.at<double>(2,0)), 
        Vector3d( tvec.at<double>(0,0), tvec.at<double>(1,0), tvec.at<double>(2,0))
    );

    // using bundle adjustment to optimize the pose
    typedef g2o::BlockSolver<g2o::BlockSolverTraits<6,2>> Block;      // parameters: 6D, error: 2D
    std::unique_ptr<Block::LinearSolverType> linearSolver(new g2o::LinearSolverDense<Block::PoseMatrixType>());
    std::unique_ptr<Block> solver_ptr(new Block(std::move(linearSolver)));
    g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg(std::move(solver_ptr));
    g2o::SparseOptimizer optimizer;
    optimizer.setAlgorithm(solver);
    // optimizer.setVerbose(true);

    // add vertex
    g2o::VertexSE3Expmap* pose = new g2o::VertexSE3Expmap();
    pose->setId(0);
    pose->setEstimate(g2o::SE3Quat(
        T_c_w_estimated_.rotation_matrix(), 
        T_c_w_estimated_.translation()
    ));     // use T from solvePnPRansac as initial guess
    optimizer.addVertex(pose);

    // add Edges
    for(int i=0; i<inliers.rows; i++)
    {
        int index = inliers.at<int>(i, 0);
        // 3D -> 2D projection
        EdgeProjectXYZ2UVPoseOnly* edge = new EdgeProjectXYZ2UVPoseOnly();
        edge->setId(i);
        edge->setVertex(0, pose);
        edge->camera_ = curr_->camera_.get();
        edge->point_ = Vector3d(pts3d[index].x, pts3d[index].y, pts3d[index].z); // point in world frame(first image frame)
        edge->setMeasurement(Vector2d(pts2d[index].x, pts2d[index].y));          // point in pixel frame
        edge->setInformation(Eigen::Matrix2d::Identity());
        optimizer.addEdge(edge);
        match_3dpts_[index]->matched_times_++;                                  // set the inlier map points
    }
    optimizer.initializeOptimization();
    optimizer.optimize(10);
    
    // g2o optimized result
    T_c_w_estimated_ = SE3 (
        pose->estimate().rotation(),
        pose->estimate().translation()
    );
}

bool VisualOdometry::checkEstimatedPose()
{
    // check if the estimated pose is good
    if(num_inliers_ < min_inliers_)
    {
        cout << "reject because inlier is too small: " << num_inliers_ << endl;
        return false;
    }
    // if the motion is too large, it is probably wrong
    SE3 T_r_c = ref_->T_c_w_ * T_c_w_estimated_.inverse();
    Sophus::Vector6d d = T_r_c.log();
    if(d.norm() > 5.0)
    {
        cout << "reject because motion is too large: " << d.norm() << endl;
        return false;
    }
    return true;
}

bool VisualOdometry::checkKeyFrame()
{
    SE3 T_r_c = ref_->T_c_w_ * T_c_w_estimated_.inverse();
    Sophus::Vector6d d = T_r_c.log();
    Vector3d trans = d.head<3>();
    Vector3d rot = d.tail<3>();
    if ( rot.norm() >key_frame_min_rot || trans.norm() >key_frame_min_trans )
        return true;
    return false;
}

void VisualOdometry::addKeyFrame()
{
    if(map_->keyframes_.empty())
    {
        // first keyframe, add all 3d points into map
        for(size_t i=0; i<keypoints_curr_.size(); i++)
        {
            double d = curr_->findDepth(keypoints_curr_[i]);
            if(d<0)
            {
                continue;
            }
            Vector3d p_world = ref_->camera_->pixel2world(
                Vector2d(keypoints_curr_[i].pt.x, keypoints_curr_[i].pt.y), curr_->T_c_w_, d
            );
            Vector3d n = p_world - ref_->getCamCenter();
            n.normalize();
            MapPoint::Ptr map_point = MapPoint::createMapPoint(
                p_world, n, descriptors_curr_.row(i).clone(), curr_.get()
            );
            map_->insertMapPoint(map_point);
        }
    }
    map_->insertKeyFrame(curr_);
    ref_ = curr_;
}

void VisualOdometry::addMapPoints()
{
    // add the new map points into map
    vector<bool> matched(keypoints_curr_.size(), false);
    for(int index:match_2dkp_index_)
    {
        matched[index] = true;
    }
    for(int i=0; i<keypoints_curr_.size(); i++)
    {
        if(matched[i] == true)
        {
            continue;
        }
        double d = ref_->findDepth(keypoints_curr_[i]);
        if(d<0)
        {
            continue;
        }
        Vector3d p_world = ref_->camera_->pixel2world(
            Vector2d(keypoints_curr_[i].pt.x, keypoints_curr_[i].pt.y),
            curr_->T_c_w_, d
        );
        Vector3d n = p_world - ref_->getCamCenter();
        n.normalize();
        MapPoint::Ptr map_point = MapPoint::createMapPoint(
            p_world, n, descriptors_curr_.row(i).clone(), curr_.get()
        );
        map_->insertMapPoint(map_point);
    }
}

void VisualOdometry::optimizeMap()
{
    // remove the hardly seen and no visible points
    for(auto iter=map_->map_points_.begin(); iter != map_->map_points_.end();)
    {
        if(!curr_->isInFrame(iter->second->pos_))   // rm the mappoints not in the current frame
        {
            iter = map_->map_points_.erase(iter);
            continue;
        }
        float match_ratio = float(iter->second->matched_times_) / iter->second->visible_times_;
        if(match_ratio < map_point_erase_ratio_)    // rm low matching ratio mappoints
        {
            iter = map_->map_points_.erase(iter);
            continue;
        }
        double angle = getViewAngle(curr_, iter->second);
        if(angle > M_PI/6)                          // rm large angle mappoints
        {
            iter = map_->map_points_.erase(iter);
            continue;
        }
        if(iter->second->good_ == false)
        {
            // TODO try triangulate this map point
        }
        iter++;
    }

    if(match_2dkp_index_.size() < 100)
    {
        addMapPoints();
    }
    if(map_->map_points_.size() > 1000)
    {
        // TODO map is too large, remove some one
        map_point_erase_ratio_ += 0.05;
    }
    else
    {
        map_point_erase_ratio_ = 0.1;
    }
    cout << "map points: " << map_->map_points_.size() << endl;
}

double VisualOdometry::getViewAngle ( Frame::Ptr frame, MapPoint::Ptr point )
{
    Vector3d n = point->pos_ - frame->getCamCenter();
    n.normalize();
    return acos( n.transpose()*point->norm_ );
}

}