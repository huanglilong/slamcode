#include <Eigen/Core>
#include <Eigen/Dense>
#include <ctime>
#include <iostream>

using namespace std;

#define MATRIX_SIZE 50

int main(int argc, char **argv) {
  Eigen::Matrix<float, 2, 3> matrix_23f;
  Eigen::Vector3d v_3d;
  Eigen::Matrix<float, 3, 1> vd_3f;

  Eigen::Matrix3d matrix_33d = Eigen::Matrix3d::Zero();
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> matrix_dynamic;
  Eigen::MatrixXd matrix_x;

  matrix_23f << 1, 2, 3, 4, 5, 6;
  cout << matrix_23f << endl;
  /*
    matrix_23f = 
    [1, 2, 3]
    [4, 5, 6]
  */
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 3; j++) {
      cout << matrix_23f(i, j) << "\t";
    }
    cout << endl;
  }

  v_3d << 3, 2, 1;
  vd_3f << 4, 5, 6;
  Eigen::Matrix<double, 2, 1> result = matrix_23f.cast<double>() * v_3d;    // data type convertion, float -> double
  cout << result << endl;

  Eigen::Matrix<float, 2, 1> result2 = matrix_23f * vd_3f;
  cout << result2 << endl;

  matrix_33d = Eigen::Matrix3d::Random();
  cout << matrix_33d << endl << endl;

  cout << matrix_33d.transpose() << endl;
  cout << matrix_33d.sum() << endl;
  cout << matrix_33d.trace() << endl;
  cout << 10 * matrix_33d << endl;
  cout << matrix_33d.inverse() << endl;
  cout << matrix_33d.determinant() << endl;

  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(
      matrix_33d.transpose() * matrix_33d);
  cout << "Eigen values = \n" << eigen_solver.eigenvalues() << endl;
  cout << "Eigen vectors = \n" << eigen_solver.eigenvectors() << endl;

  Eigen::Matrix<double, MATRIX_SIZE, MATRIX_SIZE> matrix_NN;
  matrix_NN = Eigen::MatrixXd::Random(MATRIX_SIZE, MATRIX_SIZE);
  Eigen::Matrix<double, MATRIX_SIZE, 1> v_Nd;
  v_Nd = Eigen::MatrixXd::Random(MATRIX_SIZE, 1);

  clock_t time_stt = clock();
  Eigen::Matrix<double, MATRIX_SIZE, 1> x = matrix_NN.inverse() * v_Nd;
  cout << "time use in normal inverse is "
       << 1000 * (clock() - time_stt) / (double)CLOCKS_PER_SEC << "ms" << endl;

  time_stt = clock();
  x = matrix_NN.colPivHouseholderQr().solve(v_Nd);
  cout << "time use in Qr decomposition is "
       << 1000 * (clock() - time_stt) / (double)CLOCKS_PER_SEC << "ms" << endl;

  return 0;
}