#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/video/tracking.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        cout << "usage: useLK path_to_dataset" << endl;
        return 1;
    }
    string path_to_dataset = argv[1];
    string associate_file = path_to_dataset + "/associate.txt";

    ifstream fin(associate_file);
    if(!fin)
    {
        cerr << "Cann't find associate.txt!" << endl;
        return 1;
    }

    string rgb_file, depth_file, time_rgb, time_depth;
    list<cv::Point2f> keypoints;
    cv::Mat color, depth, last_color;

    for(int index=0; index<100; index++)
    {
        fin >> time_rgb >> rgb_file >> time_depth >> depth_file;
        color = cv::imread(path_to_dataset+"/"+rgb_file);
        depth = cv::imread(path_to_dataset+"/"+depth_file, -1); // load origin file, this info not use in the code
        if(index == 0)
        {
            // extract first image FAST features
            vector<cv::KeyPoint> kps;
            cv::Ptr<cv::FastFeatureDetector> detector = cv::FastFeatureDetector::create();
            detector->detect(color, kps);
            for(auto kp:kps)
            {
                keypoints.push_back(kp.pt);
            }
            last_color = color;
            continue;           // extract first image feature for tracking
        }
        if(color.data == nullptr || depth.data == nullptr)
        {
            continue;   // invaild image or depth
        }
        // use LK tracking others image's features
        vector<cv::Point2f> next_keypoints;
        vector<cv::Point2f> prev_keypoints;
        for(auto kp:keypoints)
        {
            prev_keypoints.push_back(kp);
        }
        vector<unsigned char> status;
        vector<float> error;
        cv::calcOpticalFlowPyrLK(last_color, color, prev_keypoints, next_keypoints, status, error);

        // delete lost keypoints
        int i=0;
        for(auto iter=keypoints.begin(); iter!=keypoints.end(); i++)
        {
            if(status[i] == 0)
            {
                iter = keypoints.erase(iter);
                continue;
            }
            *iter = next_keypoints[i];      // save the tracking keypoint in keypoints
            iter++;
        }
        cout << "tracked keypoints: " << keypoints.size() << endl;
        if(keypoints.size() == 0)
        {
            cout << "all keypoints are lost." << endl;
            break;
        }
        // draw keypoints
        cv::Mat img_show = color.clone();
        for(auto kp:keypoints)
        {
            cv::circle(img_show, kp, 4, cv::Scalar(0,240,0), 1);
        }
        cv::imshow("corners", img_show);
        cv::waitKey(0);
        last_color = color;
    }
    return 0;
}