#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <chrono>
#include <ctime>
#include <climits>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <g2o/core/base_vertex.h>
#include <g2o/core/base_unary_edge.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/dense/linear_solver_dense.h>
#include <g2o/core/robust_kernel.h>
#include <g2o/types/sba/types_six_dof_expmap.h>

using namespace std;
using namespace cv;
using namespace g2o;

// 
struct Measurement
{
    Measurement(Eigen::Vector3d p, float g) : pos_world(p), grayscale(g) {}
    Eigen::Vector3d pos_world;
    float grayscale;
};

inline Eigen::Vector3d project2Dto3D(int x, int y, int d, float fx, float fy, float cx, float cy, float scale)
{
    float zz = float(d) / scale;
    float xx = zz * (x-cx) / fx;
    float yy = zz * (y-cy) / fy;
    return Eigen::Vector3d(xx, yy, zz);
}

inline Eigen::Vector2d project3Dto2D(float x, float y, float z, float fx, float fy, float cx, float cy)
{
    float u = fx * x / z + cx;
    float v = fy * y / z + cy;
    return Eigen::Vector2d(u, v);
}

// direct method estimate camera pose
// input: point in world frame with grayscale, new grayscale image; output: camera pose
bool poseEstimationDirect(const vector<Measurement>& measurements, cv::Mat* gray, Eigen::Matrix3f& instrinsics, Eigen::Isometry3d& Tcw);

// project a 3d point into a image plane, the error is photometric error
// an unary edge with one vertex SE3Expmap (the pose of camera)
class EdgeSE3ProjectDirect: public BaseUnaryEdge<1, double, VertexSE3Expmap>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    EdgeSE3ProjectDirect() {}
    EdgeSE3ProjectDirect(Eigen::Vector3d point, float fx, float fy, float cx, float cy, cv::Mat* image)
        : x_world_(point), fx_(fx), fy_(fx), cx_(cx), cy_(cy), image_(image)
    {}
    virtual void computeError()
    {
        const VertexSE3Expmap* v = static_cast<const VertexSE3Expmap*> (_vertices[0]);
        Eigen::Vector3d x_local = v->estimate().map(x_world_);      // x_local in camera frame
        float ux = x_local[0]*fx_/x_local[2] + cx_;                 // x,y in pixel frame
        float vy = x_local[1]*fy_/x_local[2] + cy_;
        // check u, v is in the image, out of image
        if((ux-4)<0 || (ux+4)>image_->cols || (vy-4)<0 || (vy+4)>image_->rows)
        {
            _error(0,0) = 0.0;
            this->setLevel(1);
        }
        else
        {
            _error(0,0) = getPixelValue(ux,vy) - _measurement;
        }
    }

    // plus in manifold
    virtual void linearizeOplus()
    {
        if(level() == 1)
        {
            _jacobianOplusXi = Eigen::Matrix<double, 1, 6>::Zero();
            return;
        }
        VertexSE3Expmap* vtx = static_cast<VertexSE3Expmap*> (_vertices[0]);
        Eigen::Vector3d xyz_trans = vtx->estimate().map(x_world_);              // q in book, in camera frame
        
        double x = xyz_trans[0];
        double y = xyz_trans[1];
        double invz = 1.0/xyz_trans[2];
        double invz_2 = invz * invz;

        float u = x * fx_ * invz + cx_;
        float v = y * fy_ * invz + cy_;

        // jacobian from se3 to u,v
        // Note that in g2o the Lie algebra is (\omege, \epsilon), where \omege is so(3) and \epsilon the translation
        Eigen::Matrix<double, 2, 6> jacobian_uv_ksai;
        jacobian_uv_ksai(0,0) = -x*y*invz_2*fx_;
        jacobian_uv_ksai(0,1) = (1+(x*x*invz_2))*fx_;
        jacobian_uv_ksai(0,2) = -y*invz*fx_;
        jacobian_uv_ksai(0,3) = invz*fx_;
        jacobian_uv_ksai(0,4) = 0;
        jacobian_uv_ksai(0,5) = -x*invz_2*fx_;

        jacobian_uv_ksai(1,0) = -(1+y*y*invz_2)*fy_;
        jacobian_uv_ksai(1,1) = x*y*invz_2*fy_;
        jacobian_uv_ksai(1,2) = x*invz*fy_;
        jacobian_uv_ksai(1,3) = 0;
        jacobian_uv_ksai(1,4) = invz*fy_;
        jacobian_uv_ksai(1,5) = -y*invz_2*fy_;

        Eigen::Matrix<double, 1, 2> jacobian_pixel_uv;
        jacobian_pixel_uv(0,0) = (getPixelValue(u+1,v)-getPixelValue(u-1,v))/2;
        jacobian_pixel_uv(0,1) = (getPixelValue(u,v+1)-getPixelValue(u,v-1))/2;
        _jacobianOplusXi = jacobian_pixel_uv * jacobian_uv_ksai;
    }

    virtual bool read(std::istream& in) {}
    virtual bool write(std::ostream& out) const {}

protected:
    // get a gray scale vaule from reference image(bilinear interpolated)
    inline float getPixelValue(float x, float y)
    {
        uchar* data = & image_->data[int(y) * image_->step + int(x)];
        float xx = x - floor(x);
        float yy = y - floor(y);
        return float (
                   ( 1-xx ) * ( 1-yy ) * data[0] +
                   xx* ( 1-yy ) * data[1] +
                   ( 1-xx ) *yy*data[ image_->step ] +
                   xx*yy*data[image_->step+1]
               );
    }

public:
    Eigen::Vector3d x_world_;           // 3D point in world frame
    float cx_=0, cy_=0, fx_=0, fy_=0;   // camera intrinsics
    cv::Mat* image_=nullptr;
};

int main(int argc, char** argv)
{
    if ( argc != 2 )
    {
        cout<<"usage: direct_sparse path_to_dataset"<<endl;
        return 1;
    }
    srand ( ( unsigned int ) time ( 0 ) );
    string path_to_dataset = argv[1];
    string associate_file = path_to_dataset + "/associate.txt";

    ifstream fin ( associate_file );

    string rgb_file, depth_file, time_rgb, time_depth;
    cv::Mat color, depth, gray;
    vector<Measurement> measurements;
    // 相机内参
    float cx = 325.5;
    float cy = 253.5;
    float fx = 518.0;
    float fy = 519.0;
    float depth_scale = 1000.0;
    Eigen::Matrix3f K;
    K<<fx,0.f,cx,0.f,fy,cy,0.f,0.f,1.0f;

    Eigen::Isometry3d Tcw = Eigen::Isometry3d::Identity();

    cv::Mat prev_color;
    // first image use as reference, other image use direct method with reference image
    for(int index=0; index<10; index++)
    {
        cout<<"*********** loop "<<index<<" ************"<<endl;
        fin>>time_rgb>>rgb_file>>time_depth>>depth_file;
        color = cv::imread ( path_to_dataset+"/"+rgb_file );
        depth = cv::imread ( path_to_dataset+"/"+depth_file, -1 );
        if ( color.data==nullptr || depth.data==nullptr )
            continue; 
        cv::cvtColor ( color, gray, cv::COLOR_BGR2GRAY );

        if(index == 0)
        {
            // select the pixels with high gradiants
            for(int x=10; x<gray.cols-10; x++)
            {
                for(int y=10; y<gray.rows-10; y++)
                {
                    Eigen::Vector2d delta(
                        gray.ptr<uchar>(y)[x+1] - gray.ptr<uchar>(y)[x-1],
                        gray.ptr<uchar>(y+1)[x] - gray.ptr<uchar>(y-1)[x]
                    );
                    if(delta.norm() < 50)
                    {
                        continue;
                    }
                    ushort d = depth.ptr<ushort>(y)[x];
                    if(d==0)
                    {
                        continue;
                    }
                    Eigen::Vector3d p3d = project2Dto3D ( x, y, d, fx, fy, cx, cy, depth_scale );
                    float grayscale = float(gray.ptr<uchar>(y)[x]);
                    measurements.push_back(Measurement(p3d, grayscale));
                }
            }
            prev_color = color.clone();
            cout << "add total " << measurements.size() << " measurement." << endl;
            continue;
            // Direct Sparse Method
            // vector<cv::KeyPoint> keypoints;
            // cv::Ptr<cv::FastFeatureDetector> detector = cv::FastFeatureDetector::create();
            // detector->detect(color, keypoints);
            // for(auto kp:keypoints)
            // {
            //     // 去掉邻近边缘处的点
            //     if ( kp.pt.x < 20 || kp.pt.y < 20 || ( kp.pt.x+20 ) >color.cols || ( kp.pt.y+20 ) >color.rows )
            //         continue;
            //     ushort d = depth.ptr<ushort> ( cvRound ( kp.pt.y ) ) [ cvRound ( kp.pt.x ) ];
            //     if ( d==0 )
            //         continue;
            //     Eigen::Vector3d p3d = project2Dto3D( kp.pt.x, kp.pt.y, d, fx, fy, cx, cy, depth_scale ); // point in first image camera frame
            //     float grayscale = float(gray.ptr<uchar> ( cvRound ( kp.pt.y ) ) [ cvRound ( kp.pt.x ) ]);
            //     measurements.push_back(Measurement(p3d, grayscale));
            // }
            // prev_color = color.clone();
            // continue;
        }

        // direct method estimate motion
        poseEstimationDirect(measurements, &gray, K, Tcw);
        cout << "Tcw=" << Tcw.matrix() << endl;

        // plot the features points
        cv::Mat img_show(color.rows*2, color.cols, CV_8UC3);
        prev_color.copyTo(img_show(cv::Rect(0,0,color.cols, color.rows)));
        color.copyTo(img_show(cv::Rect(0,color.rows,color.cols, color.rows)));
        for(Measurement m:measurements)
        {
            if(rand() > RAND_MAX/5)
            {
                continue;
            }
            Eigen::Vector3d p = m.pos_world;
            Eigen::Vector2d pixel_prev = project3Dto2D(p( 0,0 ), p( 1,0 ), p( 2,0 ), fx, fy, cx, cy);
            Eigen::Vector3d p2 = Tcw*m.pos_world;
            Eigen::Vector2d pixel_now = project3Dto2D( p2 ( 0,0 ), p2 ( 1,0 ), p2 ( 2,0 ), fx, fy, cx, cy );
            if ( pixel_now(0,0)<0 || pixel_now(0,0)>=color.cols || pixel_now(1,0)<0 || pixel_now(1,0)>=color.rows )
                continue;
            
            float b = 255*float(rand())/RAND_MAX;
            float g = 255*float(rand())/RAND_MAX;
            float r = 255*float(rand())/RAND_MAX;
            cv::circle(img_show, cv::Point2d(pixel_prev(0,0), pixel_prev(1,0)), 4, cv::Scalar(b,g,r), 2);
            cv::circle(img_show, cv::Point2d(pixel_now(0,0), pixel_now(1,0)+color.rows), 4, cv::Scalar(b,g,r), 2);
            // cv::line(img_show, cv::Point2d(pixel_prev(0,0), pixel_prev(1,0)), cv::Point2d(pixel_now(0,0), pixel_now(1,0)+color.rows), cv::Scalar(b,g,r),1);
        }
        cv::imshow("result", img_show);
        cv::waitKey(0);
    }
    return 0;
}

bool poseEstimationDirect(const vector<Measurement>& measurements, cv::Mat* gray, Eigen::Matrix3f& K, Eigen::Isometry3d& Tcw)
{
    // init g2o
    typedef g2o::BlockSolver<g2o::BlockSolverTraits<6,1>> DirectBlock;      // parameters: 6D, error: 1D
    std::unique_ptr<DirectBlock::LinearSolverType> linearSolver(new g2o::LinearSolverDense<DirectBlock::PoseMatrixType>());
    std::unique_ptr<DirectBlock> solver_ptr(new DirectBlock(std::move(linearSolver)));
    g2o::OptimizationAlgorithmLevenberg* solver = new g2o::OptimizationAlgorithmLevenberg(std::move(solver_ptr));
    g2o::SparseOptimizer optimizer;
    optimizer.setAlgorithm(solver);
    optimizer.setVerbose(true);

    g2o::VertexSE3Expmap* pose = new g2o::VertexSE3Expmap();
    pose->setEstimate(g2o::SE3Quat(Tcw.rotation(), Tcw.translation()));
    pose->setId(0);
    optimizer.addVertex(pose);

    // add edges
    int id=1;
    for(Measurement m:measurements)
    {
        EdgeSE3ProjectDirect* edge = new EdgeSE3ProjectDirect(
            m.pos_world,
            K ( 0,0 ), K ( 1,1 ), K ( 0,2 ), K ( 1,2 ), gray
        );
        edge->setVertex(0, pose);
        edge->setMeasurement(m.grayscale);
        edge->setInformation(Eigen::Matrix<double,1,1>::Identity());
        edge->setId(id++);
        optimizer.addEdge(edge);
    }
    cout << "edge in graph: " << optimizer.edges().size() << endl;
    optimizer.initializeOptimization();
    optimizer.optimize(30);
    Tcw = pose->estimate();
}
